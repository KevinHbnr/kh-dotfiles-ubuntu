#!/bin/bash

#assign version variables
JAVA_VERSION="8"
NODE_VERSION="8"
DOCKER_COMPOSE_VERSION="1.14.0"
INTELLIJ_COMMUNITY_VERSION="2017.1.5"
GIT_MAIL_ADDRESS=""
GIT_USERNAME=""
SSH_FILENAME="id_rsa"
LOGFILE="./post-install-ubuntu.log"

# Wipe LOGFILE if you don't want to add to it at each run
rm -f "$LOGFILE"

# run script for installing apps
echo ""
echo "===================="
echo " INSTALLING APPS! "
echo "===================="
echo ""

sh ./scripts/install-apps.sh \
    $JAVA_VERSION \
    $NODE_VERSION \
    $DOCKER_COMPOSE_VERSION \
    $INTELLIJ_COMMUNITY_VERSION 2>&1 | tee -a $LOGFILE

echo ""
echo "===================="
echo " INSTALLED APPS! "
echo "===================="
echo ""

# run script for configuring system and apps
echo ""
echo "===================="
echo " CONFIGURING SYSTEM AND APPS! "
echo "===================="
echo ""

sh ./scripts/configure-stuff.sh $GIT_MAIL_ADDRESS $GIT_USERNAME $SSH_FILENAME 2>&1 | tee -a $LOGFILE

echo ""
echo "===================="
echo " CONFIGURED SYSTEM AND APPS! "
echo "===================="
echo ""

echo ""
echo " PLEASE CHECK ./post-install-ubuntu-log.txt FOR ERRORS! "
echo ""

echo ""
echo "==================="
echo " PLEASE REBOOT AFTER INSTALLING OH MY ZSH! "
echo "==================="
echo ""

# oh my zsh has to be installed as last step cos a bug in the install.sh script exits...
echo "INSTALLING OH MY ZSH ON TOP OF ZSH"
curl -L http://install.ohmyz.sh > install.sh
sh install.sh

echo ""
echo "==================="
echo " PLEASE REBOOT AFTER INSTALLING OH MY ZSH! "
echo "==================="
echo ""

