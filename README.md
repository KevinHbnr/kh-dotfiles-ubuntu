# My dotfiles for ubuntu desktop #

Got started scripting my post install configuration for my ubuntu (currently 16.04 LTS) desktop. 
It's just the first steps right now and there will be added more things in future.

### How do I run it? ###
Right now it's just one bash script. The script assumes you've got a fresh install of ubuntu.
If you have downloaded the script, there are some version and user variables in the first paragraph of the script that have to be set.
After you did this, just run it by cd'ing to root directory of this project and running `sh ./post-install-ubuntu.sh`

### What does it? ###
Current state of the script does the following described steps. Apps that have no version as a variable set up always download the latest stable version.

#### Installed apps ####
* Chrome browser, 
* All OpenJDK and JRE packages that are useful for development (version that has been set as a variable) 
* Maven 
* Git
* TLP and all TLP related packages that optimise power usage for Lenovo ThinkPads
* zsh and Oh my zsh
* IntelliJ Community Edition in version that has to be set up as variable
* Curl
* nodejs, build-essential and npm
* Visual Studio Code
* docker
* docker-compose (with completions for zsh) WARNING not tested in actual post-install situation when zsh isn't the used shell already

#### Configuration ####
Furthermore there are some last config steps done...
* Setting mail and username in git config, as well as generating a private ssh key and adding it to the ssh-agent, so it can be manually uploaded to github or similar providers later
* Switching default shell to zsh
* defaultly disabling bluetooth on system startup
* adjusting apps locked to launcher: throwing out firefox and all those libre office tools and adding chrome, gedit, visual studio code

### Open ToDos ###
* Test if docker-compose zsh completion works before switching shell to zsh
* Fixing bugs in locking apps to launcher
* Include actual dotfiles for applications
* Check if variables are empty strings (or ask the user directly when running script)
* Make execution idempotent
* Parse online resources for latest stable version of intellij and/or java and node
* TLP configs
* Sys configs (e.g. closing lid behaviour, keyboard backlight)