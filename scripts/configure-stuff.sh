#!/bin/bash

# assign variables
GIT_MAIL_ADDRESS=$1
GIT_USERNAME=$2
SSH_FILENAME=$3

if [[ -z $GIT_MAIL_ADDRESS || -z $GIT_USERNAME || -z $SSH_FILENAME]];
then
    echo `date`" - Missing mandatory arguments: GIT_MAIL_ADDRESS and or GIT_USERNAME and or SSH_FILENAME. "
    echo `date`" - Usage: ./scripts/configure-stuff.sh  [GIT_MAIL_ADDRESS] [GIT_USERNAME] [SSH_FILENAME]. "
    exit 1
fi

# git config
echo "CONFIGURE GIT"
git config --global user.email $GIT_MAIL_ADDRESS
git config --global user.name $GIT_USERNAME

# generate ssh private and public key pair
echo "GENERATING SSH PRIVATE AND PUBLIC KEY PAIR"
ssh-keygen -t rsa -b 4096 -C $GIT_MAIL_ADDRESS -f ~/.ssh/$SSH_FILENAME
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/$SSH_FILENAME

# disable bluetooth on system startup
echo "DISABLE BLUETOOTH DEFAULTLY ON SYSTEM STARTUP"
if [ "`tail -n1 /etc/rc.local`" != "exit 0" ]; then
	echo "Cannot disable bluetooth on startup cause last line in /etc/rc.local isn't exit 0, so I cannot append something before exit 0."
else
	sudo sed -i -e '$i rfkill block bluetooth\n' /etc/rc.local
fi

# set items locked to launcher
echo "SET ITEMS LOCKED TO LAUNCHER"
gsettings set com.canonical.Unity.Launcher favorites \
"['application://ubiquity.desktop', 'application://org.gnome.Nautilus.desktop', 'application://google-chrome.desktop','application://unity-control-center.desktop', 'unity://running-apps', 'unity://expo-icon', 'unity://devices']"

