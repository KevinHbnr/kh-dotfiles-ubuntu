#!/bin/bash

# assign variables
JAVA_VERSION=$1
NODE_VERSION=$2
DOCKER_COMPOSE_VERSION=$3
INTELLIJ_COMMUNITY_VERSION=$4

if [[ -z $JAVA_VERSION || -z $NODE_VERSION || -z $INTELLIJ_COMMUNITY_VERSION]];
then
    echo `date`" - Missing mandatory arguments: JAVA_VERSION and or NODE_VERSION and or INTELLIJ_COMMUNITY_VERSION. "
    echo `date`" - Usage: ./scripts/install-apps.sh  [JAVA_VERSION] [NODE_VERSION] [INTELLIJ_COMMUNITY_VERSION]. "
    exit 1
fi

# add repos
echo "ADDING APT-GET REPOS"
sudo wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sudo wget -O - https://deb.nodesource.com/setup_$NODE_VERSION.x | sudo -E bash -
sudo sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" \
>> /etc/apt/sources.list.d/google-chrome.list'
sudo add-apt-repository ppa:linrunner/tlp
sudo add-apt-repository -y "deb https://packages.microsoft.com/repos/vscode stable main"
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EB3E94ADBE1229CF
wget -q -0 - https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository \    
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -
echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list


# basic update
echo "UPDATING AND UPGRADING APT-GET"
sudo apt-get -y --force-yes update
sudo apt-get -y --force-yes upgrade

# install apps
echo "INSTALLING APPS"
sudo apt-get install -y \
	google-chrome-stable \
	openjdk-$JAVA_VERSION-jre \
	icedtea-$JAVA_VERSION-plugin \
	openjdk-$JAVA_VERSION-jdk \
	openjdk-$JAVA_VERSION-demo \
	openjdk-$JAVA_VERSION-doc \
	openjdk-$JAVA_VERSION-jre-headless \
	openjdk-$JAVA_VERSION-source \
	maven \
	git \
	tlp \
	tlp-rdw \
	tp-smapi-dkms \
	acpi-call-dkms \
	zsh \
	curl \
	nodejs \
	yarn \
	build-essential \
	code \
	docker-ce

# changing symlink to node
echo "CHANGING SYMLINK FROM NODEJS TO NODE"
sudo ln -s `which nodejs` /usr/bin/node

# install docker-compose
sudo sh -c "curl -L https://github.com/docker/compose/releases/download/$DOCKER_COMPOSE_VERSION/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
sudo chmod +x /usr/local/bin/docker-compose

# install command completion for zsh for docker-compose
mkdir -p ~/.zsh/completion
curl -L https://raw.githubusercontent.com/docker/compose/master/contrib/completion/zsh/_docker-compose > ~/.zsh/completion/_docker-compose
fpath=(~/.zsh/completion $fpath)
autoload -Uz compinit && compinit -i

# test installed apps
echo "TEST INSTALLED APPS"
java -version
mvn -v
git --version
node -v
npm -v
docker -v
docker-compose --version

# install intellij
echo "INSTALLING INTELLIJ COMMUNITY $INTELLIJ_COMMUNITY_VERSION"
cd /usr/local/
mkdir idea
cd /usr/local/idea
sudo wget -O /tmp/intellij.tar.gz http://download.jetbrains.com/idea/ideaIC-$INTELLIJ_COMMUNITY_VERSION.tar.gz 
sudo tar xfz /tmp/intellij.tar.gz 
cd idea-IC-*/bin 
./idea.sh

# tidying up
sudo rm -r node
